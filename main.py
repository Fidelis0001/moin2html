import os
from wikimarkup.parser import Parser   # pip install py-wikimarkup
import re
import argparse
from shutil import copyfile

parser = argparse.ArgumentParser(description="Parse a moin page dump")

parser.add_argument("-s", dest="src_path", default=r"C:\Users\gangs\DFD\freedom days\dfd2017\dfd2017\core\data\pages",
                    help="Location of the path containing the page folders")
parser.add_argument("-t", dest="target_path", default=r"C:\Users\gangs\DFD\freedom days\wiki",
                    help="Path storing the output results")
parser.add_argument("-o", dest="online_path", default="\images",
                    help="Base path where images will be found online")
parser.add_argument("-d", dest="detail", default="debug",
                    help="Options: minimal, hide_skipped, show_skipped, debug")

args = parser.parse_args()

src_path = str(args.src_path)
target_path = str(args.target_path)
online_path = str(args.online_path)

if args.detail == 'hide_skipped':
    show_skipped = False
    show_copied = True
    show_errors = True
elif args.detail == 'show_skipped':
    show_skipped = True
    show_copied = True
    show_errors = True
elif args.detail == 'debug':
    show_skipped = True
    show_copied = True
    show_errors = True
    # more options can follow later
else:  # assuming minimal
    show_skipped = False
    show_copied = False
    show_errors = False

directory = os.fsencode(src_path)
skipped_counter = 0
copied_counter = 0


def hex_to_utf8(match):
    hex_string = match.group(1)
    utf8_string = ''

    # Split hex string into 2-char elements and convert to UTF-8
    for i in range(0, len(hex_string), 2):
        hex_char = hex_string[i:i + 2]
        utf8_string += bytes.fromhex(hex_char).decode('utf-8', 'replace')

    return utf8_string


def replace_hex_codes(s):
    # Regular expression pattern to find hexadecimal codes within parentheses
    pattern = re.compile(r'\(([0-9a-fA-F]+)\)')

    # Substitute found hex codes with their UTF-8 characters
    result_string = pattern.sub(hex_to_utf8, s)
    return result_string


def markup2html(content):
    parser = Parser()
    html = parser.parse(content, show_toc=False)
    return html


def convert_wiki_links_to_html(html_content, base_url=""):
    # Function to remove unwanted list elements from HTML
    def remove_unwanted_lists(match):
        list_content = match.group(0)
        # Check if the list elements are at the beginning of the HTML
        if html_content.startswith(list_content):
            return ''  # Remove the entire list
        return list_content

    # Regular expression pattern to find wiki links
    wiki_link_pattern = re.compile(r'\[\[([^\]|]+)(?:\|([^\]]+))?\]\]')

    # Function to convert wiki link match objects to HTML links
    def replace_wiki_link(match):
        page_name = re.sub('<[^<]+?>', '', match.group(1))
        try:
            link_text = re.sub('<[^<]+?>', '', match.group(2)) or page_name
        except TypeError as e:
            link_text = page_name
        except Exception as e:
            if show_errors:
                print(">> FAIL:", type(e))
            link_text = page_name
        href = base_url + page_name.replace(' ', '_') + '\index.html'
        return f'<a href="{href}">{link_text}</a>'

    # Regular expression pattern to find wiki attachments (images usually)
    wiki_attachments_pattern = re.compile(r'\{\{attachment: ?([^}]+)\}\}')

    # Function to convert wiki attachment to HTML image links
    def replace_wiki_attachments(match):
        image_parts = match.group(1).split("|", 2)
        image_name = image_parts[0]
        image_alt = image_parts[1] if len(
            image_parts) > 1 else image_name.split(".")[0]
        image_style = "style='" + \
            image_parts[2] + ";'" if len(image_parts) > 2 else ""

        # Attachments can have a comment field and markup too, divided by pipe symbols |
        # First block (always present) is filename, second is alt text, third and onward are CSS
        # Example: {{attachment:myLogo.png|My Organisation|width=100%,align="center"}}

        # copy image from attachments to new folder location
        from_path = os.path.join(src_path, file.decode("utf-8"), "attachments")
        to_path = os.path.join(target_path, filename)
        href_path = online_path + filename + image_name
        if os.path.isfile(os.path.join(from_path, image_name)):
            try:
                if not os.path.exists(to_path):
                    os.makedirs(to_path)
                copyfile(os.path.join(from_path, image_name), os.path.join(to_path, image_name))
            except Exception as e:
                if show_errors:
                    print(">> SKIPPED FILE", os.path.join(from_path, image_name), "reason: ", e)

        return f'<img src="{href_path}" alt="{image_alt}" {image_style}/>'

    # Replace wiki links with HTML links
    html_with_links = wiki_link_pattern.sub(replace_wiki_link, html_content)
    html_with_images = wiki_attachments_pattern.sub(
        replace_wiki_attachments, html_with_links)

    # Remove unwanted lists from the HTML
    html_without_lists = re.sub(
        r'<ol>.*?</ol>', remove_unwanted_lists, html_with_images, flags=re.DOTALL)

    return html_without_lists


def get_current(filepath):
    current_path = os.path.join(filepath, "current")
    if not os.path.isfile(current_path):
        if show_errors:
            print(f">> SKIPPED {filepath}. Reason: No 'current' file.")
        return False

    try:
        with open(current_path) as current:
            raw_content = current.read()
            print("DEBUG: Raw Content:", repr(raw_content))  # Debug print
            # Check if unwanted content is at the beginning of the HTML
            if raw_content.startswith('<ol><li>'):
                raw_content = re.sub(
                    r'<ol>.*?</ol>', '', raw_content, flags=re.DOTALL)
            revision = raw_content.strip()
    except Exception as e:
        if show_errors:
            print(f">> Failed to get current version for {filepath}. Reason: {e}")
        return False

    live_version = os.path.join(filepath, 'revisions', revision.replace('/', os.path.sep))
    if os.path.isfile(live_version):
        with open(live_version, encoding='utf-8') as markup_file:
            markup = markup_file.read()
            print("DEBUG: Original Markup:", repr(markup))  # Debug print
            html = markup2html(markup)
            print("DEBUG: Converted HTML:", repr(html))  # Debug print
            html_with_links = convert_wiki_links_to_html(html)

            return html_with_links
    else:
        if show_errors:
            print(f">> Failed to find live version for {filepath}.")

        return False


def cleanup_name(moinfile):
    cleaned = os.fsdecode(moinfile)
    cleaned = replace_hex_codes(cleaned)
    return cleaned


for file in os.listdir(directory):
    filename = cleanup_name(file)
    print("Processing:", filename)  # Print the processed filename
    source = os.path.join(src_path, os.fsdecode(file))
    html_content = get_current(source)
    if html_content:
        try:
            os.makedirs(os.path.join(target_path, filename), exist_ok=True)
        except FileExistsError:
            pass
        target = os.path.join(target_path, filename, "index.html")
        with open(target, 'w', encoding='utf-8') as target_file:
            target_file.write(html_content)

        copied_counter += 1
        if show_copied:
            print(">> COPIED", source, "as HTML in", target)
            print("   Adjusted HTML written to:", target)  # Print the adjusted HTML file path
    else:
        skipped_counter += 1
        if show_skipped:
            print(">> SKIPPED", source)

print(">> SUMMARY: Copied", copied_counter,
      "pages and skipped", skipped_counter, "pages.")
